quick-run:*.go
	go build -o quick-run -ldflags '-s -w'
install:quick-run
	cp quick-run /usr/local/bin/
	cp quick-run.png /usr/share/icons/
	cp quick-run.desktop /usr/share/applications/

uninstall:
	rm /usr/local/bin/quick-run
	rm /usr/share/icons/quick-run.png
	rm /usr/share/applications/quick-run.desktop
