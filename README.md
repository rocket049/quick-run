## 这是一个工具软件，用于快速启动常用的图形界面程序。
它的功能包括：

- 从PATH快速搜索程序
- 搜索目录中的可执行程序
- 保存历史记录，按照最近使用时间优先排序

预编译版本只提供ubuntu22.04版本

自行编译方法：

- 安装`go`编译器
- `make`
- `sudo make install`

编译期间如果出现错误报告：缺少`xxx.h`头文件，就用命令`apt install libxxx-dev`安装相应的软件包，字母全部小写。

### 怎么安装 go 编译器
先到 [https://go.dev](https://go.dev) 下载编译器压缩包`go1.18.n.linux-amd64.tar.gz`，安装方法以`linux`为例，顺序使用下面的命令完成`go`编译器的安装：

```
cd /usr/local
sudo tar xvfz ~/下载/go1.18.n.linux-amd64.tar.gz
sudo ln -s /usr/local/go/bin/go /usr/local/bin/
#设置国内的 GOPROXY 代理服务器
go env -w GOPROXY=https://goproxy.cn,direct
```
